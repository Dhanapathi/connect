var express = require('express');
var router = express.Router();
var ScheduleMeeting = require('../models/schedule_meeting.js');

// Add new meeting scheduled
router.post('/addSchedule', function( req, res ) {
    console.log("inside add schedule");
    var schedule = {
        meeting_with: req.body.meeting_with,
        meeting_from: req.body.meeting_from,
        meeting_time: req.body.meeting_time
    };
    console.log(schedule);
    ScheduleMeeting.addSchedule( schedule, function( scheduled ) {
        if( scheduled !== null ) {
            res.statusCode = 200;
            res.json( scheduled );
        } else {
            res.send('false');
        }
    });
});

// Get all meetings scheduled with current user
router.post('/self', function( req, res ) {
    var email = req.body.email;
    
    ScheduleMeeting.getSelfScheduledList( email, function( meetingsList ) {
        if( meetingsList !== null ) {
            res.statusCode = 200
            res.json( meetingsList );
        } else {
            res.send('false');
        }
    });
});

// Get all meetings scheduled by current user
router.post('/others', function( req, res ) {
    var email = req.body.email;
    
    ScheduleMeeting.getOthersScheduledList( email, function( meetingsList ) {
        if( meetingsList !== null ) {
            res.statusCode = 200
            res.json( meetingsList );
        } else {
            res.send('false');
        }
    });
});

// Get all meetings scheduled between two users
router.post('/getAllSchedules', function( req, res ) {
    var schedule = {
        first_user: req.body.first_user,
        second_user: req.body.second_user
    };
    
    ScheduleMeeting.getScheduledListBetweenUsers( schedule, function( meetingsList ) {
        if( meetingsList !== null ) {
            res.statusCode = 200
            res.json( meetingsList );
        } else {
            res.send('false');
        }
    });
});

module.exports = router;