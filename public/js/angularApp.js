var ConnectApp = angular.module("Connect", ['ngRoute', 'angularjs-datetime-picker']);

ConnectApp.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.

	when('/', {
		templateUrl : 'public/views/login.html',
		controller : 'LoginController'
	}).

	when('/login', {
		templateUrl : 'public/views/login.html',
		controller : 'LoginController'
	}).
    
    when('/register', {
		templateUrl : 'public/views/register.html',
		controller : 'LoginController'
	}).

	when('/addProfile', {
		templateUrl : 'public/views/addprofile.html',
		controller : 'ProfileController'
	}).
	when('/home/profile/addEmployment', {
		templateUrl : 'public/views/addemployment.html',
		controller : 'ProfileController'
	}).

 when('/home/profile/changeProfilePic', {
       templateUrl : 'public/views/addprofilepic.html',
       controller : 'ProfilePicController'
       }).

	when('/home/profile/addEducation', {
		templateUrl : 'public/views/addeducation.html',
		controller : 'ProfileController'
	}).
	when('/home/profile/addSkill', {
		templateUrl : 'public/views/addskill.html',
		controller : 'ProfileController'
	}).

    when('/home', {
		templateUrl : 'public/views/home.html',
		controller : 'HomeController'
	}).

	 when('/home/remoteprofile/:id', {
		templateUrl : 'public/views/remoteprofile.html',
		controller : 'RemoteProfileController'
	}).


    when('/home/profile', {
		templateUrl : 'public/views/profile.html',
		controller : 'ProfileController'
	}).
	when('/home/connect', {
		templateUrl : 'public/views/connect.html',
		controller : 'ConnectController'
	}).
    
	when('/logout', {
		templateUrl : 'public/views/login.html',
		controller : 'LoginController'
	}).

	otherwise({
		redirectTo : '/'
	});

} ]);


var compareTo = function() {
    return {
      require: "ngModel",
      scope: {
        otherModelValue: "=compareTo"
      },
      link: function(scope, element, attributes, ngModel) {

        ngModel.$validators.compareTo = function(modelValue) {
          return modelValue == scope.otherModelValue;
        };

        scope.$watch("otherModelValue", function() {
          ngModel.$validate();
        });
      }
    };
  };


  ConnectApp.directive("compareTo", compareTo);