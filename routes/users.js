var express = require('express');
var router = express.Router();

var User = require('../models/user.js');
var Profile = require('../models/profile.js');

// Register a new user
router.post('/register', function( req, res ) {
    var newUser = {
        email: req.body.email,
        password: req.body.password
    };
    
    User.addUser( newUser, function( user ) {
        if( user === null ) {
            res.send('false');
        } else {
            res.json( user );
        }
    });
});

// Get user by email
router.post('/userByEmail', function( req, res ) {
    var email = req.body.email;
    
    User.getUser( email, function( userData ) {
        if( userData !== null ){
            res.send( userData );
        } else {
            res.send('false');
        }
    });
});

// Get all users
router.get('/users', function( req, res ) {
    User.getAllUsers( function( userList ) {
        if( userList !== null ) {
            res.json( userList );
        } else {
            res.send('false');
        }
    });
});

// Change password
router.post('/changePassword', function( req, res ) {
    
    if( req.body.newPassword === req.body.confirmPassword ) {
        if( req.body.password === req.body.newPassword ){
            
        } else {
            var user = {
                email: req.body.email,
                password: req.body.password,
                newPassword: req.body.newPassword
            };
            
            User.changePassword( user, function( updatedUser ) {
                if( updatedUser === null ) {
                    res.send('false');
                } else {
                    res.send( updatedUser );
                }
            });
        }
    }
});

module.exports = router;