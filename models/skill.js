var Sequelize = require('sequelize');
var sequelize = require('../config/dbconfig.js');

/**
* Skills Schema
*/
var Skill = sequelize.define('skill', {
    id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: Sequelize.INTEGER(30),
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    skill_name: {
      type: Sequelize.STRING,
      allowNull: true
    },
    description: {
      type: Sequelize.STRING,
      allowNull: true
    },
    last_used: {
      type: Sequelize.DATE,
      allowNull: true
    },
    experience: {
      type: Sequelize.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'skills'
  }
);

sequelize.sync();
module.exports = Skill;

// Add new Skill
module.exports.addUserSkill = function( skill, callback ) {
    var newSkill = {
        user_id: skill.user_id,
        skill_name: skill.skill_name,
        description: skill.description,
        last_used: skill.last_used,
        experience: skill.experience
    };
    
    Skill.create( newSkill ).then( callback ).catch( function( error ) {
        console.log('Database Error while adding new skill : ' + error );
    });
};

// Update existing skill
module.exports.updateSkill = function( id, userId, skill, callback ) {
    var query = {
        id: id,
        user_id: userId
    };
    
    var updateData = {
        skill_name: skill.skill_name,
        description: skill.description,
        last_used: skill.last_used,
        experience: skill.experience
    };
    
    Skill.update( updateData, {
        where: query
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while updating skill : ' + error );
    });
};

// Remove skill
module.exports.deleteSkill = function( id, userId, callback ) {
    var query = {
        id: id,
        user_id: userId
    };
    
    Skill.destroy({
        where: query
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while deleteing skill : ' + error );
    });
};

// Get skill by id
module.exports.getSkill = function( id, callback ) {
    Skill.findOne({
        where: {
            id: id
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting a skill : ' + error );
    });
};

//Get skills of a user
module.exports.getUserSkills = function( userId, callback )
{
    Skill.findAll({
        where: {
            user_id: userId
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting user skills : ' + error );
    });
};

// Get skills by skill_name
module.exports.getSkillByName = function( skillName, callback ) {
    Skill.findAll({
        where: {
            skill_name: skillName
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting skill by skill_name : ' + error );
    });
};