var express = require('express');
var router = express.Router();
var io = require('socket.io');

var User = require('../models/user.js');
var Profile  =require('../models/profile.js');

// Authenticate User
router.post('/login', function( req, res ) {
    User.getUser( req.body.email, function( user ) {
        if(  user !== null ) {
            if( user.password === req.body.password ) {
                req.session.user = {
                    id: user.id,
                    username: user.email
                };
                var session_username = req.body.email;

                Profile.getProfile( user.id, function( existingUser ) {
                    if( existingUser !== null ) {
                        console.log("%%%%%%%%%%%%%%%%%%%%%%%%%??????????????????????????######   " );
                        var userStatus = {
                            status: 'success',
                            profile: 'yes',
                            user : existingUser
                        
                        };

                        res.json( userStatus );
                    } else {
                        var userStatus = {
                            status: 'success',
                            profile: 'no'
                        };

                        res.json( userStatus );
                    }
                });
            } else {
                res.json({status:'failed',message :"Invalid password"});
            }
        } else {
            res.json({status:'failed',message :"Invalid username or password"});
        }
    });
});

// Validate user
router.get('/validate', function( req, res ) {
    var userId = req.session.user.id;
    
    Profile.getProfile( userId, function( profile ) {
        if( profile !== null ) {
            res.send('success');
        } else {
            res.send('false');
        }
    });
});

// Logout
router.get('/logout', function( req, res ) {
         
        Profile.updateStatus( req.session.user.id, "offline", function( updatedProfile ) {
        if( updatedProfile !== null ) {
            req.session.destroy();
           
            res.json({'logout': "success"});
            
        } else {
            res.json({'logout': "failed"});
        }
    });
        
   // io.emit("online_users", "logout");
});

module.exports = router;