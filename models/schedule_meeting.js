var Sequelize = require('sequelize');
var sequelize = require('../config/dbconfig.js');

/**
* ScheduledMeetings Schema
*/
var ScheduleMeeting = sequelize.define('schedule_meeting', {
    id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    recruiter_id: {
      type: Sequelize.STRING,
      allowNull: false
    },
    user_id: {
      type: Sequelize.STRING,
      allowNull: false
    },
    meeting_time: {
      type: Sequelize.DATE,
      allowNull: false
    }
  }, {
    tableName: 'schedule_meetings'
  }
);

sequelize.sync();
module.exports = ScheduleMeeting;

// Add new Scheduled Meeting data
module.exports.addSchedule = function( schedule, callback ) {
    var newSchedule = {
        recruiter_id: schedule.meeting_with,
        user_id: schedule.meeting_from,
        meeting_time: schedule.meeting_time
    };
    
    sequelize.sync();
    ScheduleMeeting.create( newSchedule ).then( callback ).catch( function( error ) {
        console.log('Database Error while creating meeting schedule : ' + error );
    });
};

// Get all meeting schedules by time
module.exports.getAllSchedules = function( time, callback ) {
    ScheduleMeeting.findAll({
        where: {
            meeting_time: time
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting all schedules : ' + error );
    });
};

// Get all meetings scheduled with current user
module.exports.getSelfScheduledList = function( email, callback ) {
    ScheduleMeeting.findAll({
        where: {
            recruiter_id: email
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting meetings scheduled with current user : ' + error );
    });
};

// Get all meetings scheduled by current user
module.exports.getOthersScheduledList = function( email, callback ) {
    ScheduleMeeting.findAll({
        where: {
            user_id: email
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting meetings scheduled by current user : ' + error );
    });
};

// Get all meetings scheduled between two users
module.exports.getScheduledListBetweenUsers = function( schedule, callback ) {
    var query = 'SELECT * FROM schedule_meetings where ( recruiter_id = "' + schedule.first_user + '" or user_id = "' + schedule.first_user + '") and ( recruiter_id = "' + schedule.second_user + '" or user_id = "' + schedule.second_user + '")';
    
    sequelize.query( query, {
        type: sequelize.QueryTypes.SELECT
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting meetings scheduled between two users : ' + error );
    });
};

module.exports.getMeetingSchedule = function(scheduleTime, callback) {
    ScheduleMeeting.findAll({
        where: {
            meeting_time : scheduleTime
        }
    }).then(callback).catch(callback);
}