ConnectApp.factory('Auth', function($http, $q) {

	var authFactory = {};

	// api call for login
	authFactory.login = function(username, password) {
		return $http.post('/auth/login', {
			email : username,
			password : password
		}).success(function(data,status) {
			console.log(data);
			return data;
		}).error(function(data,err){
			data = "invalid";
			return data;
		})
	};

	//api call for logout
	authFactory.logout = function () {
		  return $http.get('/auth/logout')
		};

	// Api call for  register	
	authFactory.register = function(username, password) {
		return $http.post('/api/user/register', {
			email : username,
			password : password
		}).success(function(data,status) {
			console.log(data);
			return data;
		}).error(function(data,err){
			data = "invalid";
			return data;
		})
	};

    return authFactory;
});
