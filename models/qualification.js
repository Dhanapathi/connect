var Sequelize = require('sequelize');
var sequelize = require('../config/dbconfig.js');

/**
* Qualification Schema
*/
var Qualification = sequelize.define('qualification', {
    id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: Sequelize.INTEGER(30),
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    university: {
      type: Sequelize.STRING,
      allowNull: true
    },
    degree: {
      type: Sequelize.STRING,
      allowNull: true
    },
    start_date: {
      type: Sequelize.DATE,
      allowNull: true
    },
    end_date: {
      type: Sequelize.DATE,
      allowNull: true
    },
    city: {
      type: Sequelize.STRING,
      allowNull: true
    },
    country: {
      type: Sequelize.STRING,
      allowNull: true
    },
    description: {
      type: Sequelize.STRING,
      allowNull: true
    }
  }, {
    tableName: 'qualifications'
  }
);

sequelize.sync();
module.exports = Qualification;

// Get qualification by Id
module.exports.getQualification = function( id, callback ) {
    Qualification.findOne({
        where: {
            id: id
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting qualification : ' + error );
    });
};

// Get All Qualifications of a user
module.exports.getAllQualifications = function( userId, callback ) {
    Qualification.findAll({
        where: {
            user_id: userId
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting users qualifications : ' + error );
    });
};

// Get qualification by degree
module.exports.getQualificationByDegree = function( degree, callback ) {
    Qualification.findAll({
        where: {
            degree: degree
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting qualification by degree : ' + error ); 
    });
};

// Add new qualification
module.exports.addQualification = function( qualification, callback ) {
    var newQualification = {
        user_id: qualification.user_id,
        university: qualification.university,
        degree: qualification.degree,
        start_date: qualification.start_date,
        end_date: qualification.end_date,
        city: qualification.city,
        country: qualification.country,
        description: qualification.description
    };
    
    sequelize.sync();
    Qualification.create( newQualification ).then( callback ).catch( function( error ) {
        console.log('Database Error while adding new qualification : ' + error );
    });
};

// Update existing qualification
module.exports.updateQualification = function( id, userId, qualification, callback ) {
    
    var query = {
        id: id,
        user_id: userId
    };
    
    var updateData = {
        university: qualification.university,
        degree: qualification.degree,
        start_date: qualification.start_date,
        end_date: qualification.end_date,
        city: qualification.city,
        country: qualification.country,
        description: qualification.description
    };
    
    Qualification.update( updateData, {
        where: query
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while updating qualification : ' + error );
    });
};

// Remove Qualification
module.exports.deleteQualification = function( id, userId, callback ) {
    var query = {
        id: id,
        user_id: userId
    };
    
    Qualification.destroy({
        where: query
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while deleting qualification : ' + error );
    });
};