var Sequelize = require('sequelize');
var sequelize = require('../config/dbconfig.js');

/**
*   Connection Schema
**/
var Connection = sequelize.define('connection', {
    id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: Sequelize.INTEGER(30),
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    con_user_id: {
      type: Sequelize.INTEGER(30),
      allowNull: false
    }
   
    
  }, {
    tableName: 'connections'
  }
);

sequelize.sync();
module.exports = Connection;

// Get connections
module.exports.getAllConnections = function( userId, callback ) {
    
     var qury = "select distinct p.user_id, p.first_name, p.last_name, p.id, p.status from profiles as p inner join connections as c on c.con_user_id = p.user_id where c.user_id = " + userId + ";"
     sequelize.query(qury, {
        type: sequelize.QueryTypes.SELECT
    }).then(callback).catch(function(err) {
        console.log("this is err : " + err)
    });

};

// Add new connection
module.exports.addConnection = function( userId, connectionId, callback ) {
    var newConnection = {
        user_id: userId,
        con_user_id: connectionId
    };
    
    sequelize.sync();
    Connection.create( newConnection ).then( callback ).catch( function( error ) {
        console.log('Database Error while creating a new employment : ' + error );
    });
};

