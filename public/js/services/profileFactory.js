ConnectApp.factory('Profile', function($http, $q) {

	var profileFactory = {};

	// APi call for add profile information 
	profileFactory.addProfile = function(profile) {
		return $http.post('/api/profile/newProfile', {
			first_name : profile.first_name, 
			last_name : profile.last_name,
            middle_name : profile.middle_name,
            gender: profile.gender,
            date_of_birth: profile.date_of_birth,
            phone:profile.phone,
            address: profile.address,
            pincode: profile.pincode,
            city: profile.city,
            country: profile.country,
            designation: profile.designation,
            about_me:profile.about_me,
            industry:profile.industry
        
    })
	};

	// api call for add user skills
    profileFactory.addSkill = function(skill) {
		return $http.post('/api/skill/addSkill', {
			skill_name : skill.name, 
			description : skill.description,
            last_used : skill.last_used,
            experience: skill.experience

    })
	};

	// api call for add employment
    profileFactory.addEmployment = function(emp) {
		return $http.post('/api/employment/addEmployment', {
			company : emp.company, 
			start_date : emp.start_date,
            end_date : emp.end_date,
			city: emp.city,
            country: emp.country,
            description : emp.description
    })
	};

// api call for add education or qualification
 profileFactory.addEducation = function(edu) {
		return $http.post('/api/qualification/addQualification', {
			university : edu.university, 
			degree : edu.degree,
			start_date: edu.start_date,
            end_date : edu.end_date,
			city: edu.city,
            country: edu.country,
            description : edu.description
    })
	};

	// api call for user profile content
	 profileFactory.getProfile = function() {
		return $http.get('/api/profile')
	};

	 profileFactory.getEmployments = function() {
		return $http.post('/api/employment/getUserEmployments')
	};

	profileFactory.getSkills = function() {
		return $http.post('/api/skill/getUserSkills');
	};

	profileFactory.getEducations = function() {
		return $http.post('/api/qualification/getQualifications')
	};

	// APi call for search the profile
	profileFactory.searchProfile = function(searchText) {
		return $http.post('/api/profile/getSearchPeople',{
			searchText : searchText
		})
	};

	// Api calls for remote profile content 
	 profileFactory.getRemoteProfile = function(userid) {
		return $http.post('/api/profile/remoteProfile',
		{
			userId : userid
		})
	};

	 profileFactory.getRemoteEmployments = function(userid) {
		return $http.post('/api/employment/getUserEmployments',
		{
			user_id : userid
		})
	};

	profileFactory.getRemoteSkills = function(userid) {
		return $http.post('/api/skill/getUserSkills',
		{
			user_id : userid	
		});
	};

	profileFactory.getRemoteEducations = function(userid) {
		return $http.post('/api/qualification/getQualifications',
		{
			user_id : userid
		})
	};


	//api for add connection(connect)

	profileFactory.addConnection = function (userid) {
		return $http.post('/api/connection/addConnection',{
			connId : userid
		})
	};


	// getting all list of user connections
	profileFactory.getAllConnection = function () {
		return $http.post('/api/connection/getUserConnections')
	};


	//getting tokbox apikey,token,session
	profileFactory.getTokbox = function () {
		return $http.get('/api/tokbox/session')
	};

	//Post method tokbox apikey,token,session
	profileFactory.postTokbox = function (sid) {
		return $http.post('/api/tokbox/session',{sessionId : sid })
	};


    return profileFactory;
});


