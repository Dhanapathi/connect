ConnectApp.factory('tokbox', function($http, $q,$rootScope) {

	var tokFactory = {};

  
var session;
var publisher;
var currentSteam;
	//api call for logout
	tokFactory.session = function (apiKey, sessionId) {


    session = OT.initSession(apiKey, sessionId);

    session.on('streamCreated', function(event) {
      currentSteam = event.stream;
    session.subscribe(event.stream, 'subscriber', {
      insertMode: 'append',
      width: '100%',
      height: '100%'
    });
    

    console.log($rootScope.callingSpinner);
     

     $rootScope.$apply(function() {
    $rootScope.callingSpinner = true;
});
     console.log($rootScope.callingSpinner);
    console.log("subscriber......event");
    
  });

  session.on("streamDestroyed", function (event) {
  console.log("Stream stopped. Reason: " + event.reason);
  //console.log(event.stream);
//session.unsubscribe(event.stream);
  session.disconnect();
   $rootScope.$apply(function() {
    $rootScope.callingButton = true;
});
  
});

  session.on('sessionDisconnected', function(event) {
    console.log('You were disconnected from the session.', event.reason);
  // session.unsubscribe(currentSteam);
  session.disconnect();
  $rootScope.$apply(function() {
    $rootScope.callingButton = true;
});
  });

		 return session;
     //token = token;
		};

  tokFactory.connect = function (session,token) {
      session.connect(token, function(error) {
    // If the connection is successful, initialize a publisher and publish to the session
    if (!error) {
      var pubOptions = {publishAudio:true, publishVideo:false};
       publisher = OT.initPublisher('publisher',pubOptions, {
        insertMode: 'append',
        width: '100%',
        height: '100%'
      });

      session.publish(publisher);
      console.log("Punblisher......is calling");
    } else {
      console.log('There was an error connecting to the session: ', error.code, error.message);
    }
  });
    
  }

  tokFactory.disconnect = function(){
   session.disconnect();
  }
tokFactory.pubAudio=function(param){
  publisher.publishAudio(param);
}

tokFactory.pubVideo = function(param){
publisher.publishVideo(param);
}



    return tokFactory;
});
