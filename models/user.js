var Sequelize = require('sequelize');
var sequelize = require('../config/dbconfig.js');
var Profile = require('../models/profile.js');

/**
* User Schema
*/
var User = sequelize.define('user', {
    id: {
      type: Sequelize.INTEGER(30),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    role: {
      type: Sequelize.STRING,
      allowNull: true
    },
    status: {
      type: Sequelize.INTEGER(11),
      allowNull: false, 
      defaultValue: 1
    }
  }, {
    tableName: 'users'
  }
);

sequelize.sync();
module.exports = User;

// Get user by username
module.exports.getUser = function( username, callback ){
    User.findOne({
        where: {
            email: username
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting a user : ' + error );
    });
};

// Get all users
module.exports.getAllUsers = function( callback ) {
    User.findAll().then( callback ).catch( function( error ) {
        console.log('Database Error while getting all users : ' + error );
    });
};

// Create a new user
module.exports.addUser = function( user, callback ) {
    var newUser = {
        email: user.email,
        password: user.password
    };
    
    sequelize.sync();
    User.create( newUser ).then( callback ).catch( function ( error ) {
        console.log('Database Error while creating new user : ' + error );
    });
};

// Change password
module.exports.changePassword = function( user, callback ) {
    var query = {
        email: user.email,
        password: user.password
    };
    
    var updateData = {
        password: user.newPassword
    };
    
    sequelize.sync();
    User.update( updateData, { where: query }).then( callback ).catch( function( error ) {
        console.log('Databse Error while updating password : ' + error );
    });
}