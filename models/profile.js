var Sequelize = require('sequelize');
var sequelize = require('../config/dbconfig.js');

/**
* Profile Schema
*/

var Profile = sequelize.define('profile', {
    id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: Sequelize.INTEGER(30),
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    first_name: {
      type: Sequelize.STRING,
      allowNull: true
    },
    middle_name: {
      type: Sequelize.STRING,
      allowNull: true
    },
    last_name: {
      type: Sequelize.STRING,
      allowNull: true
    },
    gender: {
      type: Sequelize.STRING,
      allowNull: true
    },
    date_of_birth: {
      type: Sequelize.DATE,
      allowNull: true
    },
    phone: {
      type: Sequelize.STRING(20),
      allowNull: false
    },
    address: {
      type: Sequelize.STRING,
      allowNull: true
    },
    pincode: {
      type: Sequelize.INTEGER(10),
      allowNull: true
    },
    city: {
      type: Sequelize.STRING,
      allowNull: true
    },
    country: {
      type: Sequelize.STRING,
      allowNull: true
    },
    designation: {
      type: Sequelize.STRING,
      allowNull: true
    },
    about_me: {
      type: Sequelize.STRING,
      allowNull: true
    },
    industry: {
      type: Sequelize.STRING,
      allowNull: true
    },
    status: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: "online"
    }
  }, {
    tableName: 'profiles'
  }
);

sequelize.sync();
module.exports = Profile;

// Create new profile
module.exports.addProfile = function( userId, profile, callback) {
    var newProfile = {
        user_id: userId,
        first_name: profile.first_name,
        last_name: profile.last_name,
        middle_name: profile.middle_name,
        gender: profile.gender,
        date_of_birth: profile.date_of_birth,
        phone: profile.phone,
        address: profile.address,
        pincode: profile.pincode,
        city: profile.city,
        country: profile.country,
        designation: profile.country,
        about_me: profile.about_me,
        industry: profile.industry
    };
    
    sequelize.sync();
    Profile.create( newProfile ).then( callback ).catch( function( error ) {
        console.log('Database Error while creating new profile : ' + error );
    }); 
};

// Update existing profile
module.exports.updateProfile = function( userId, profile, callback) {
    var query = {
        user_id: userId
    };
    
    var updateProfileData = {
        first_name: profile.first_name,
        last_name: profile.last_name,
        middle_name: profile.middle_name,
        gender: profile.gender,
        date_of_birth: profile.date_of_birth,
        phone: profile.phone,
        address: profile.address,
        pincode: profile.pincode,
        city: profile.city,
        country: profile.country,
        designation: profile.country,
        about_me: profile.about_me,
        industry: profile.industry
    };
    
    sequelize.sync();
    Profile.update( updateProfileData, { where: query }).then( callback ).catch( function( error ) {
        console.log('Database Error while updating profile : ' + error );
    });
};
//update status
module.exports.updateStatus = function( userId, newstatus, callback) {
    var query = {
        user_id: userId
    };
    
    var updateProfileData = {
        
        status: newstatus
    };
    
    sequelize.sync();
    Profile.update( updateProfileData, { where: query }).then( callback ).catch( function( error ) {
        console.log('Database Error while updating profile : ' + error );
    });
};
// Get Profile by userId
module.exports.getProfile = function( userId, callback ) {
    Profile.findOne({
        where: {
            user_id: userId
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting a profile : ' + error );
    });
};

// Get all profiles
module.exports.getAllProfiles = function( callback ) {
    Profile.findAll().then( callback ).catch( function( error ) {
        console.log('Database Error while getting all profiles : ' + error );
    });
};

// Get profiles by location
module.exports.getProfilesByLocation = function( location , callback ) {
    Profile.findAll({
        where: {
            city: location
        }
    }).then( callback ).catch( function( error ) {
        console.log('Databse Error while getting all profiles by location : ' + error );
    });
};

// Search People by location skill and name
module.exports.getSearchPeople = function(profile, callback) {

    var qury = "select distinct p.user_id, p.first_name, p.last_name, p.id from profiles as p left outer join skills as s on p.user_id = s.user_id left outer join employments as e on p.user_id = e.user_id where p.first_name like '%" + profile.search_data + "%' or p.last_name like '%" + profile.search_data + "%' or p.city like '%" + profile.search_data + "%' or s.skill_name like '%" + profile.search_data + "%' or e.company like '%" + profile.search_data + "%'";

    sequelize.query(qury, {
        type: sequelize.QueryTypes.SELECT
    }).then(callback).catch(function(err) {
        console.log("this is err : " + err)
    });
};

