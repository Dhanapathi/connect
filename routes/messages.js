var express = require('express');
var router = express.Router();
var Message = require('../models/message.js');

//var messages = [];

// Add new message
// router.post('/', function( req, res ) {
//     var message = {
//         sender_id: req.body.sender_id,
//         reciever_id: req.body.reciever_id,
//         sent_msg: req.body.sent_msg,
//         is_read: req.body.is_read
//     };
//
//     Message.addMessage( message, function( storedMessage ) {
//         if( storedMessage !== null ) {
//             res.json( storedMessage );
//         } else {
//             res.send('false');
//         }
//     });
// });

var addMessage = function(message, callback) {
  console.log("inside message route");
  var msg = {
      sender_id: message.sender_id,
      reciever_id: message.reciever_id,
      sent_msg: message.sent_msg
  }
  Message.addMessage(message, function(err, response) {
    if(err) {
      callback(err, null);
    } else {
      callback(null, "success");
    }
  });
}
//
// // Get message by sender and reciever id
router.post('/getMessage', function( req, res ) {
    var message = {
        sender_id: req.body.sender_id,
        reciever_id: req.body.reciever_id
    };

    Message.getMessages( message, function( messages ) {
        if( messages !== null ) {
            res.json( messages );
        } else {
            res.send('false');
        }
    });
});
//
// // Get all messages data
// router.post('/getAllMessages', function( req, res ) {
//     var message = {
//         sender_id: req.body.sender_id
//     };
//
//     Message.getAllMessages( message, function( messages ) {
//         if( messages !== null ) {
//             res.json( messages );
//         } else {
//             res.send('false');
//         }
//     });
// });
//
// // Get searched users chat messages data
// router.post('/getSearchChat', function( req, res ) {
//     var message = {
//         sender_id: req.body.sender_id,
//         reciever_id: req.body.reciever_id
//     };
//
//     Message.getSearchChat( message, function( messages ) {
//         if( messages !== null ) {
//             res.json( messages );
//         } else {
//             res.send('false');
//         }
//     });
// });
//
// // Getting all sender users data
// router.post('/getAllSenderUsers', function( req, res ) {
//     var message = {
//         email: req.body.email
//     };
//
//     Message.getAllSenderUsers( message, function( senderList ) {
//         if( senderList !== null ) {
//             res.json( senderList );
//         } else {
//             res.send('false');
//         }
//     });
// });
//
//Update message read status
router.post('/updateReadStatus', function( req, res ) {
    var message = {
        sender_id: req.body.sender_id,
        reciever_id: req.body.reciever_id
    };

    Message.updateReadStatus( message, function( status ) {
        if( status !== null ) {
            res.json( status );
        } else {
            res.send('false');
        }
    });
});
//
// // Getting unread message count
router.post('/unreadCount', function( req, res ) {
    var message = {
         sender_id: req.body.sender_id,
        reciever_id: req.body.reciever_id
    };

    Message.getUnreadCount( message, function( count ) {
        if( count !== null ) {
            res.json( count );
        } else {
            res.send('false');
        }
    });
});
//
// // Getting unread messages of a user
router.post('/getUnreadMessage', function( req, res ) {
    var message = {
        reciever_id: req.body.reciever_id
    };

    Message.getUnreadMessages( message, function( messages) {
        if( messages !== null ) {
            res.json( messages );
        } else {
            res.send('false');
        }
    });
});

 module.exports = {router :router,  addMessage:addMessage};
