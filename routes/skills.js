var express = require('express');
var router = express.Router();
var Skill = require('../models/skill.js');

// Add new skill
router.post('/addSkill', function( req, res ) {
    var userId = req.session.user.id;
    
    var newSkill = {
        user_id: userId,
        skill_name: req.body.skill_name,
        description: req.body.description,
        last_used: req.body.last_used,
        experience: req.body.experience
    };
    
    Skill.addUserSkill( newSkill, function( skill ) {
        if( skill !== null ) {
            res.json( skill );
        } else {
            res.send('false');
        }
    });
});

// Update existing skill
router.post('/updateSkill/:id', function( req, res ) {
    var skillId = req.params.id;
    var userId = req.session.user.id;
    
    var skill = {
        skill_name: req.body.skill_name,
        description: req.body.description,
        last_used: req.body.last_used,
        experience: req.body.experience
    };
    
    Skill.updateSkill( skillId, userId, skill, function( updatedSkill ) {
        if( updatedSkill !== null ) {
            res.json( updatedSkill );
        } else {
            res.send('false');
        }
    });
});

// Removing a skill
router.get('/deleteSkill/:id', function( req, res ) {
    var id = req.params.id;
    var userId = req.session.user.id;
    
    Skill.deleteSkill( id, userId, function( deletedSkill ) {
        if( deletedSkill !== null ) {
            res.json( deletedSkill );
        } else {
            res.send('false');
        }
    });
});

//Get skills of a user
router.post('/getUserSkills', function( req, res ) {
    var userId = req.body.user_id || req.session.user.id;
    
    Skill.getUserSkills( userId, function( skills ) {
        if( skills !== null ) {
            res.json( skills );
        } else {
            res.send('false');
        }
    });
});

module.exports = router;