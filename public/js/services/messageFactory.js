ConnectApp.factory('Msg', function($http, $q) {

	var msgFactory = {};

	// api call for getting messages
	msgFactory.getMsg = function(sender_id, reciever_id) {
		return $http.post('/api/message/getMessage', {
			sender_id : sender_id,
			reciever_id : reciever_id
		})
	};

	// api for Unread msg
	msgFactory.getUnreadMsg = function(reciever_id) {
		return $http.post('/api/message/getUnreadMessage', {
		
			reciever_id : reciever_id
		})
	};

// api for update read msg
	msgFactory.updateReadmsg = function(sender_id,reciever_id) {
		return $http.post('/api/message/updateReadStatus', {
		    sender_id : sender_id,
			reciever_id : reciever_id
		});
	};

	msgFactory.setSchedule = function(schedule) {
		return $http.post('/api/schedule/addSchedule', schedule);
	}

    return msgFactory;
});
