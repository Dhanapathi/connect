var express = require('express');
var router = express.Router();
var Employment = require('../models/employment.js');

// Get employments
router.get('/getEmployment', function( req, res ) {
    var id = req.body.id;
    
    Employment.getEmployment( id, function( employment ) {
        if( employment !== null ) {
            res.json( employment );
        } else {
            res.send('false');
        }
    });
});

// Get All Employments of a user
router.post('/getUserEmployments', function( req, res ) {
    var userId = req.body.user_id || req.session.user.id;

    Employment.getAllEmployments( userId, function( employmentList ) {
        if( employmentList !== null ) {
            res.json( employmentList );
        } else {
            res.send('false');
        }
    });
});

// Add a new employment
router.post('/addEmployment', function( req, res ) {
    var userId = req.session.user.id;
    
    var newEmplyment = {
        company: req.body.company,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        city: req.body.city,
        country: req.body.country,
        description: req.body.description
    };
    
    Employment.addEmployment( userId, newEmplyment, function( employment ) {
        if( employment !== null ) {
            res.json( employment );
        } else {
            res.send('false');
        }
    });
});

// Update existing employment
router.post('/updateEmployment', function( req, res ) {
    var id = req.body.id;
    var userId = req.session.user.id;

    var updateData = {
        company: req.body.company,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        city: req.body.city,
        country: req.body.country,
        description: req.body.description
    };

    Employment.updateEmployment( id, userId, updateData, function( updatedEmployment) {
        if( updatedEmployment !== null ) {
            res.json( updatedEmployment ); 
        } else {
            res.send('false');
        }
    });
});

// Remove employment
router.get('/delete/:id', function( req, res ) {
    var id = req.params.id;
    var userId = req.session.user.id;
    
    Employment.deleteEmployment( id, userId, function( employment ) {
        if( employment !== null ) {
            res.json( employment );
        } else {
            res.send('false');
        }
    });
});

module.exports = router;