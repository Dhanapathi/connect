-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

USE connect_development_schema;

-- ---
-- Table 'users'
-- Table ''''users'''' contains user credentials.
-- ---

DROP TABLE IF EXISTS `users`;
		
CREATE TABLE `users` (
  `id` INTEGER(30) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(50) NOT NULL,
  `password` VARCHAR(15) NOT NULL,
  `role` VARCHAR(20) NULL DEFAULT NULL,
  `status` INTEGER NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Table ''''users'''' contains user credentials.';

-- ---
-- Table 'employments'
-- Foreign table corresponding to "users" table.Contains employment details.
-- ---

DROP TABLE IF EXISTS `employments`;
		
CREATE TABLE `employments` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `user_id` INTEGER(30) NOT NULL,
  `company` VARCHAR(50) NULL DEFAULT NULL,
  `start_date` DATETIME NULL DEFAULT NULL,
  `end_date` DATETIME NULL DEFAULT NULL,
  `city` VARCHAR(30) NULL DEFAULT NULL,
  `country` VARCHAR(30) NULL DEFAULT NULL,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Foreign table corresponding to "users" table.Contains employ';

-- ---
-- Table 'profiles'
-- Foreign table corresponding to "users" table.Contains user profile.
-- ---

DROP TABLE IF EXISTS `profiles`;
		
CREATE TABLE `profiles` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `user_id` INTEGER(30) NOT NULL,
  `first_name` VARCHAR(30) NULL DEFAULT NULL,
  `middle_name` VARCHAR(30) NULL DEFAULT NULL,
  `last_name` VARCHAR(30) NULL DEFAULT NULL,
  `gender` VARCHAR(10) NULL DEFAULT NULL,
  `date_of_birth` DATE NULL DEFAULT NULL,
  `phone` INTEGER(20) NOT NULL,
  `address` VARCHAR(100) NULL DEFAULT NULL,
  `pincode` INTEGER(10) NULL DEFAULT NULL,
  `city` VARCHAR(20) NULL DEFAULT NULL,
  `country` VARCHAR(30) NULL DEFAULT NULL,
  `designation` VARCHAR(50) NULL DEFAULT NULL,
  `about_me` VARCHAR(255) NULL DEFAULT NULL,
  `industry` VARCHAR(50) NULL DEFAULT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Table ''profiles''.
Foreign table corresponding to "users" tab';

-- ---
-- Table 'skills'
-- Foreign table corresponding to "users" table. Contains individual user skills.
-- ---

DROP TABLE IF EXISTS `skills`;
		
CREATE TABLE `skills` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `user_id` INTEGER(30) NOT NULL,
  `skill_name` VARCHAR(30) NULL DEFAULT NULL,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  `last_used` DATETIME NULL DEFAULT NULL,
  `experience` INTEGER NULL DEFAULT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Foreign table corresponding to "users" table. Contains indiv';

-- ---
-- Table 'qualifications'
-- Foreign table corresponding to "users" table. Contains eduaction/qualification details.
-- ---

DROP TABLE IF EXISTS `qualifications`;
		
CREATE TABLE `qualifications` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `user_id` INTEGER(30) NOT NULL,
  `university` VARCHAR(50) NULL DEFAULT NULL,
  `degree` VARCHAR(50) NULL DEFAULT NULL,
  `start_date` DATETIME NULL DEFAULT NULL,
  `end_date` DATETIME NULL DEFAULT NULL,
  `city` VARCHAR(50) NULL DEFAULT NULL,
  `country` VARCHAR(50) NULL DEFAULT NULL,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Foreign table corresponding to "users" table. Contains eduac';

-- ---
-- Table 'messages'
-- Contains chat messages of all users.
-- ---

DROP TABLE IF EXISTS `messages`;
		
CREATE TABLE `messages` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `sender_id` VARCHAR(30) NOT NULL,
  `reciever_id` VARCHAR(30) NOT NULL,
  `sent_msg` VARCHAR(255) NULL DEFAULT NULL,
  `is_read` INTEGER NULL DEFAULT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Contains chat messages of all users.';

-- ---
-- Table 'schedule_meetings'
-- Foreign table corresponding to "users" table. Contains meeting timings.
-- ---

DROP TABLE IF EXISTS `schedule_meetings`;
		
CREATE TABLE `schedule_meetings` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `recruiter_id` VARCHAR(30) NOT NULL,
  `user_id` VARCHAR(30) NOT NULL,
  `meeting_time` DATETIME NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Foreign table corresponding to "users" table. Contains meeti';

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `employments` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);
ALTER TABLE `employments` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);
ALTER TABLE `profiles` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);
ALTER TABLE `skills` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);
ALTER TABLE `qualifications` ADD FOREIGN KEY (user_id) REFERENCES `users` (`id`);


-- ---
-- Triggers
-- ---

DROP trigger IF EXISTS trigger_user_insert;

CREATE TRIGGER trigger_user_insert BEFORE INSERT ON `users`
 FOR EACH ROW
 SET NEW.created_at = UTC_TIMESTAMP(), NEW.updated_at = UTC_TIMESTAMP(), NEW.id = (SELECT UUID());
 
DROP trigger IF EXISTS trigger_user_update;

CREATE TRIGGER trigger_user_update BEFORE UPDATE ON `users`
 FOR EACH ROW
 SET NEW.updated_at = UTC_TIMESTAMP();
 
DROP trigger IF EXISTS trigger_skills_insert;

CREATE TRIGGER trigger_skills_insert BEFORE INSERT ON `skills`
 FOR EACH ROW
 SET NEW.created_at = UTC_TIMESTAMP(), NEW.updated_at = UTC_TIMESTAMP(), NEW.id = (SELECT UUID());

DROP trigger IF EXISTS trigger_skills_update;

CREATE TRIGGER trigger_skills_update BEFORE UPDATE ON `skills`
 FOR EACH ROW
 SET NEW.updated_at = UTC_TIMESTAMP();

DROP trigger IF EXISTS trigger_schedule_meeting_insert;

CREATE TRIGGER trigger_schedule_meeting_insert BEFORE INSERT ON `schedule_meetings`
 FOR EACH ROW
 SET NEW.created_at = UTC_TIMESTAMP(), NEW.updated_at = UTC_TIMESTAMP(), NEW.id = (SELECT UUID());

DROP trigger IF EXISTS trigger_schedule_meeting_update;

CREATE TRIGGER trigger_schedule_meeting_update BEFORE UPDATE ON `schedule_meetings`
 FOR EACH ROW
 SET NEW.updated_at = UTC_TIMESTAMP();
 
DROP trigger IF EXISTS trigger_profiles_insert;

CREATE TRIGGER trigger_profiles_insert BEFORE INSERT ON `profiles`
 FOR EACH ROW
 SET NEW.created_at = UTC_TIMESTAMP(), NEW.updated_at = UTC_TIMESTAMP(), NEW.id = (SELECT UUID());

DROP trigger IF EXISTS trigger_profiles_update;

CREATE TRIGGER trigger_profiles_update BEFORE UPDATE ON `profiles`
 FOR EACH ROW
 SET NEW.updated_at = UTC_TIMESTAMP();

DROP trigger IF EXISTS trigger_employments_insert;

CREATE TRIGGER trigger_employments_insert BEFORE INSERT ON `employments`
 FOR EACH ROW
 SET NEW.created_at = UTC_TIMESTAMP(), NEW.updated_at = UTC_TIMESTAMP(), NEW.id = (SELECT UUID());

DROP trigger IF EXISTS trigger_employments_update;

CREATE TRIGGER trigger_employments_update BEFORE UPDATE ON `employments`
 FOR EACH ROW
 SET NEW.updated_at = UTC_TIMESTAMP();
 
DROP trigger IF EXISTS trigger_qualification_insert;

CREATE TRIGGER trigger_qualification_insert BEFORE INSERT ON `qualifications`
 FOR EACH ROW
 SET NEW.created_at = UTC_TIMESTAMP(), NEW.updated_at = UTC_TIMESTAMP(), NEW.id = (SELECT UUID());

DROP trigger IF EXISTS trigger_qualification_update;

CREATE TRIGGER trigger_qualification_update BEFORE UPDATE ON `qualifications`
 FOR EACH ROW
 SET NEW.updated_at = UTC_TIMESTAMP();
-- ---
-- Table Properties
-- ---

-- ALTER TABLE `users` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `employments` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `profiles` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `skills` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `qualifications` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `messages` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `schedule_meetings` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `users` (`id`,`email`,`password`,`role`,`status`,`created_at`,`updated_at`) VALUES
-- ('','','','','','','');
-- INSERT INTO `employments` (`id`,`user_id`,`company`,`start_date`,`end_date`,`city`,`country`,`description`,`created_at`,`updated_at`) VALUES
-- ('','','','','','','','','','');
-- INSERT INTO `profiles` (`id`,`user_id`,`first_name`,`middle_name`,`last_name`,`gender`,`date_of_birth`,`phone`,`address`,`pincode`,`city`,`country`,`designation`,`about_me`,`industry`,`created_at`,`updated_at`) VALUES
-- ('','','','','','','','','','','','','','','','','');
-- INSERT INTO `skills` (`id`,`user_id`,`skill_name`,`description`,`last_used`,`experience`,`created_at`,`updated_at`) VALUES
-- ('','','','','','','','');
-- INSERT INTO `qualifications` (`id`,`user_id`,`university`,`degree`,`start_date`,`end_date`,`city`,`country`,`description`,`created_at`,`updated_at`) VALUES
-- ('','','','','','','','','','','');
-- INSERT INTO `messages` (`id`,`sender_id`,`reciever_id`,`sent_msg`,`is_read`,`created_at`,`updated_at`) VALUES
-- ('','','','','','','');
-- INSERT INTO `schedule_meetings` (`id`,`recruiter_id`,`user_id`,`meeting_time`,`created_at`,`updated_at`) VALUES
-- ('','','','','','');