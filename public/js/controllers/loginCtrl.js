ConnectApp.controller('LoginController', function($scope, Auth, $location,socket, $rootScope) {
	$scope.message = "This page will be used to display LoginController";
  console.log("LoginController");


  // function for registration
  $scope.doRegistration = function () {

    Auth.register($scope.email,$scope.password).success(
				function(data) {
   					
					$location.path('/login');
				}).error(function(data){

					console.log("errrr"+data.error);
					$scope.errMessage = data.error;
						
					
				});
  }


  $scope.doLogin = function () {
    
    Auth.login($scope.username,$scope.userpassword).success(
				function(data) {
   				console.log(data);
           if(data.status === 'success' ){
             if(data.profile === 'yes'){
               $rootScope.uData = data.user;
               socket.emit('init',$rootScope.uData.user_id);

               $location.path('/home');
             }
             else
              $location.path('/addProfile');
           }	
           else if (data.status === 'failed'){
             $scope.showErr = true;
             $scope.errMsg = data.message;
           }
					
				}).error(function(data){

					console.log("errrr"+data.error);
					$scope.errMessage = data.error;
						
					
				});
  }

  $scope.hideErr = function () {
    $scope.showErr = false;
  }

});
