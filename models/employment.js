var Sequelize = require('sequelize');
var sequelize = require('../config/dbconfig.js');

/**
* Employment Schema
*/
var Employment = sequelize.define('employment', {
    id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: Sequelize.INTEGER(30),
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    company: {
      type: Sequelize.STRING,
      allowNull: true
    },
    start_date: {
      type: Sequelize.DATE,
      allowNull: true
    },
    end_date: {
      type: Sequelize.DATE,
      allowNull: true
    },
    city: {
      type: Sequelize.STRING,
      allowNull: true
    },
    country: {
      type: Sequelize.STRING,
      allowNull: true
    },
    description: {
      type: Sequelize.STRING,
      allowNull: true
    }
  }, {
    tableName: 'employments'
  }
);

sequelize.sync();
module.exports = Employment;

// Get employment
module.exports.getEmployment = function( id, callback ){
    Employment.findOne({
        where: {
            id: id
        }
    }).then( callback ).catch( function( error ) {
        console.log('Databse Error while getting emplyment : ' + error );
    });
};

// Get All Employments of a user
module.exports.getAllEmployments = function( userId, callback ) {
    Employment.findAll({
        where: {
            user_id: userId
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting user emplyments : ' + error );
    });
};

// Add new employment
module.exports.addEmployment = function( userId, employment, callback ) {
    var newEmplyment = {
        user_id: userId,
        company: employment.company,
        start_date: employment.start_date,
        end_date: employment.end_date,
        city: employment.city,
        country: employment.country,
        description: employment.description
    };
    
    sequelize.sync();
    Employment.create( newEmplyment ).then( callback ).catch( function( error ) {
        console.log('Database Error while creating a new employment : ' + error );
    });
};

// Update existing employment
module.exports.updateEmployment = function( id, userId, employment, callback ) {
    var query = {
        id: id,
        user_id: userId
    };
    
    var updateData = {
        company: employment.company,
        start_date: employment.start_date,
        end_date: employment.end_date,
        city: employment.city,
        country: employment.country,
        description: employment.description
    };
    
    Employment.update( updateData, { where: query }).then( callback ).catch( function( error ) {
        console.log('Database Error while updating employment : ' + error );
    });
};

// Remove employment
module.exports.deleteEmployment = function( id, userId, callback ) {
    
    Employment.destroy({
        where: {
            id: id,
        user_id: userId
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while deleting employment : ' + error );
    });
};