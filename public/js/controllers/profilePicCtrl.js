ConnectApp.controller('ProfilePicController', function($scope, ProfilePic, $location) {
  console.log("in Profile pic controller");

  $scope.upload = "webcam";

  ProfilePic.getUserMedia();

  $scope.capture = function() {
    ProfilePic.captureImg();
  }

  $scope.uploadPicCanvas = function() {
    console.log("inside upload");
    var fd = ProfilePic.getFileData();
    ProfilePic.upload(fd).success(function (data) {
      if(data == "success") {
        console.log("uploaded");
        $location.path('/home/profile');
      } else {
        console.log("some error");
      }
    });
  }

  $scope.uploadPicBrowse = function() {
    console.log("inside browser upload");
    var file = $scope.myFileBrowse;
    var fd = new FormData();
    fd.append('file', file);
    ProfilePic.upload(fd).success(function (data) {
      if(data == "success") {
        console.log("uploaded");
        $location.path('/home/profile');
      } else {
        console.log("some error");
      }
    });
  }

});

ConnectApp.directive('fileModel', ['$parse', function ($parse) {
   return {
      restrict: 'A',
      link: function(scope, element, attrs) {
         var model = $parse(attrs.fileModel);
         var modelSetter = model.assign;

         element.bind('change', function(){
            scope.$apply(function(){
               modelSetter(scope, element[0].files[0]);
            });
         });
      }
   };
}]);
