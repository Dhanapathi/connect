var nodemailer = require('nodemailer');
var directTransport = require('nodemailer-direct-transport');
var Schedule = require('./models/schedule_meeting.js');
var profile = require('./models/profile.js');
var twilio = require('twilio');
var flow = require('nimble');
var dateFormat = require('dateformat');
var d = new Date();

// Create a new REST API client to make authenticated requests against the
// twilio back end
var client = new twilio.RestClient('ACdb24bd0bb02a2a1c248807ff11ce666b', 'a11c5b804d9a4b17593f7cc18e821d51');

var myschedule;
var host_contact;
var guest_contact;
var host_name;
var guest_name;
var meeting_time;
var recruiter_id;
var user_id;

// var day = d.getUTCDate();
// var month = d.getUTCMonth() + 1;
// var year = d.getUTCFullYear(); 
// var hour = d.getUTCHours();
// var min = d.getUTCMinutes();

//console.log("time is   " + day + "  " + month + "  " + year + "  " + "  " + hour + "  " + min);

function scheduler() {
    //var time = ((day < 10) ? "0" + day : day) + "/" + ((month < 10) ? "0" + month : month) + "/" + year + " - " + ((hour < 9) ? "0" + (hour+1) : hour + 1) + ":" + ((min < 10) ? "0" + min : min);
    var now = new Date();
    var time = dateFormat(now, "UTC:yyyy-mm-dd HH:MM:ss");
    console.log(time);
    //     var schedule = {
    //     meeting_with: "1",
    //     meeting_from: "2",
    //     meeting_time: time
    // };
    //Schedule.addSchedule(schedule, function(response) {
        Schedule.getMeetingSchedule(time, function(scheduled) {
        console.log("---INSIDE SCHEDULE---");
        console.log("schedule length - " + scheduled.length);
        
        if (scheduled.length > 0) {
            console.log("---INSIDE IF---");
            myschedule = scheduled;
            task(0);
        }
     });
    //});

}

function task(i) {
    console.log("inside task");
    console.log("inside task " + myschedule.length +"---"+ i);
    if (i >= myschedule.length) {
        console.log("EXIT");
        return;
    }
    meeting_time = myschedule[i].dataValues.meeting_time;
    recruiter_id = myschedule[i].dataValues.recruiter_id;
    user_id = myschedule[i].dataValues.user_id;

    meeting_time = dateFormat(meeting_time, "UTC:dddd, mmmm dS, yyyy, h:MM TT");
    console.log(meeting_time);
    console.log(meeting_time + " time " + recruiter_id + " recruiter_id " + user_id + " user_id");

    // var s_day = parseInt(meeting_time.slice(0, 3));
    // var s_month = parseInt(meeting_time.slice(3, 6));
    // var s_year = parseInt(meeting_time.slice(6, 10));
    // var s_hour = parseInt(meeting_time.slice(13, 16));
    // var s_min = parseInt(meeting_time.slice(16, 18));

    profile.getProfile(recruiter_id, function(prof) {
        host_contact = prof.dataValues.phone;
        host_name = prof.dataValues.first_name;

        profile.getProfile(user_id, function(profi) {
            guest_contact = profi.dataValues.phone;
            guest_name = profi.dataValues.first_name;

            var html_body_host = '<h3>Hi ' + guest_name + '</h3><br>&nbsp;&nbsp;This is to remind' +
                ' you that you have scheduled a meeting with ' + host_name + ' at ' + meeting_time + ' UTC';

            var html_body_guest = '<h3>Hi ' + host_name + '</h3><br>&nbsp;&nbsp;This is to remind' +
                ' you that ' + guest_name + ' schedule a meeting with you at ' + meeting_time + ' UTC';

            var sms_body_host = 'Hey ' + guest_name + ', this is to remind you that you have ' +
                'schedule meeting with ' + host_name + ' at ' + meeting_time + ' UTC';

            var sms_body_guest = 'Hey ' + host_name + ',this is to remind you that ' +
                guest_name + ' schedule a meeting with you at ' + meeting_time + ' UTC';

            console.log(html_body_host);
            console.log(html_body_guest);
            console.log(sms_body_host);
            console.log(sms_body_guest);

            console.log("inside final " + meeting_time + " time " + recruiter_id + " recruiter_id " + user_id + " user_id");
            console.log("---" + host_name + " " + host_contact);
            console.log("---" + guest_name + " " + guest_contact);

            //sendMail(user_id, recruiter_id, html_body_guest);
            //sendMail(recruiter_id, user_id, html_body_host);
            sendSMS(host_contact, sms_body_guest, false, function() {
                console.log("1st callback");
            });
            sendSMS(guest_contact, sms_body_host, true, function() {
                console.log("in call back");
                task(i + 1);
            });
        });
    });
}

function sendMail(recruiter_id, user_id, html_body) {
    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport(directTransport({}));
    // setup e-mail data with unicode symbols
    console.log("sending mail");
    var mailOptions = {
        from: recruiter_id, // sender address
        to: user_id, // list of receivers
        subject: 'Meeting Schedule', // Subject line
        html: html_body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info) {
        console.log("mail send successfully");
        if (error) {
            console.log("error");
            return console.log(error);
        }
    });
}

function sendSMS(contact, sms_body, status, callback) {
    // Pass in parameters to the REST API using an object literal notation. The
    // REST client will handle authentication and response serialzation for you.
    console.log("sending sms");
    client.sms.messages.create({
        to: contact,
        from: '+17012039586',
        body: sms_body
    }, function(error, message) {
        // The HTTP request to Twilio will run asynchronously. This callback
        // function will be called when a response is received from Twilio
        // The "error" variable will contain error information, if any.
        // If the request was successful, this value will be "falsy"
        console.log("sms send");
        if (!error) {
            // The second argument to the callback will contain the information
            // sent back by Twilio for the request. In this case, it is the
            // information about the text messsage you just sent:
            //console.log('Success! The SID for this SMS message is:');
            //console.log(message.sid);

            //console.log('Message sent on:');
            //console.log(message.dateCreated);
        } else {
            //  console.log('Oops! There was an error.   ' + error);
        }
        callback();
    });
}

scheduler();