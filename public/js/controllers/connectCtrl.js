ConnectApp.controller('ConnectController', function($scope, tokbox,Msg, Profile, $location, socket, $rootScope) {
  console.log("ConnectController");
  //profile data function
  $scope.refreshProfiledata = function(){
  Profile.getAllConnection().success(function (data) {
    console.log(data);
    //all connect user list
     $scope.connList = data;
  })

  }
  $scope.refreshProfiledata();

 
  
  Profile.getTokbox().success(function (data) {
    console.log(data);
    $scope.tokBoxData = data;
  })
 
  $rootScope.callingButton = true;
  $rootScope.onlineUserList = [];

  $scope.vcallPopupWin = false;
 
  //socket.emit('private message',"4","hi id 4 kishor");

 socket.emit('connectpage',"x");

  socket.on('message', function (data) {
    console.log("private message event ");
    console.log(data);

    if(data.from === $scope.attendee.user_id){
      var data = {
        sender_id : data.from,
        sent_msg:data.message
      }
      $scope.messageData.push(data);
    }
    else
      $rootScope.mcount++;
  });

  socket.on('video_call_popup',function (data) {
    console.log("You have a video Call");

    $scope.vcallPopupWin = true;
    console.log($scope.vcallPopupWin);
    $scope.vcallData = data;
    console.log(data);
  })

  socket.on('reject_video_call_popup',function(data){
    console.log(data);
    $scope.disconnectVCall();

  })

  socket.on('online_users', function (data) {
    console.log("------------------online event");
    console.log(data);
    $rootScope.onlineUserList = data;
    console.log("..................t");
   // $scope.refreshProfiledata();
   Profile.getAllConnection().success(function (data) {
    console.log("refreshing list");
    //all connect user list
     $scope.connList = data;
  })
      
  });

 
  
  
  $scope.userStatusClass = function (id) {
    console.log("appling class by root scope");
    
    if( $rootScope.onlineUserList.indexOf(id) > 0){
      $scope.status = true;
    }
    else {
     $scope.status = false;
    }

      return $scope.status;
  }

$rootScope.callingSpinner = true;
// select user from connections appling corrosponding action after on click
$scope.selectUser = function (list,id) {
  $scope.selectedUser = id;
  $scope.activeMenu = id;
  $scope.attendee = list;
 $scope.dfun(id); //= list.first_name;
 $scope.updateReadStatus(id,$rootScope.uData.user_id);
}


$scope.sendMessage = function () {
  console.log($scope.messageText);

  var b = {
      sent_msg: $scope.messageText,
      sender_id  : $rootScope.uData.user_id
    }
    $scope.messageData.push(b);

  var data = {
    to : $scope.selectedUser,
    mesg : $scope.messageText,
    user_name : $rootScope.uData.first_name
  }
  socket.emit('private_message',data);
  $scope.messageText = '';
}
  
//video call fuction
  $scope.videoCall = function () {
   console.log($scope.tokBoxData.apiKey);
    $scope.session = tokbox.session($scope.tokBoxData.apiKey,$scope.tokBoxData.sessionId);
    console.log($scope.session);
    tokbox.connect($scope.session, $scope.tokBoxData.token);

     var vdata = {
    to : $scope.selectedUser,
    sessionId : $scope.tokBoxData.sessionId
  }
  socket.emit('video_call',vdata);
  $rootScope.callingButton = false;
  $rootScope.callingSpinner = false;
 
    
  }


// accept video call
   $scope.acceptCall = function () {

     Profile.postTokbox($scope.vcallData.sessionId).success(function (data) {
       $scope.tokBoxData2 = data;

       $scope.session = tokbox.session($scope.tokBoxData2.apiKey,$scope.tokBoxData2.sessionId);
    console.log($scope.session);
    tokbox.connect($scope.session, $scope.tokBoxData2.token);

     })
     $scope.vcallPopupWin = false;
     $rootScope.callingButton = false;

   }

//reject call

  $scope.rejectCall = function(){
    console.log("reject call fn");
    console.log($scope.vcallData);
    var data = {
    to : $scope.vcallData.from,
    reason :"can't attend the call"
      }
    socket.emit('reject_video_call',data);

    

    
     $scope.vcallPopupWin = false;
}
  


// .................................
  $scope.dfun = function(id){

    Msg.getMsg($rootScope.uData.user_id,id).success(function(data){
      $scope.messageData = data;
      console.log(data);
      console.log("inside dfun");
    })

    // Msg.getUnreadMsg(id,$rootScope.uData.user_id).success(function(data){
    //   console.log(data);
    // })

    
    // if(id === 4){
    //   $scope.messageData = $scope.dkishor;
    // }
    // else
    //  $scope.messageData = $scope.dnitin;

  } 

$scope.dnitin = [];
$scope.dkishor = []
  


// for genrating dynamic class in chat-box


//for img user left or right
$scope.dclass = function(id){
  
  var ids = id;
   if(typeof ids === "string"){
ids = parseInt(id);
  } 
  
  //  console.log(ids +"  "+ $scope.attendee.user_id);
  //  console.log(ids === $scope.attendee.user_id);
   
 
  // console.log( typeof ids );
  // console.log( typeof  $scope.attendee.user_id);
  if(ids === $scope.attendee.user_id ){
    
 x = "media-left";
 
  } 
  else
  x = "media-right";


  return x;
}

// for text content left or right based on user data
$scope.dspeech = function(id){

var ids = id;
   if(typeof ids === "string"){
ids = parseInt(id);
  } 

var cls;
if(ids !== $scope.attendee.user_id ){
  cls = "speech-right"
}

return cls;

}


//disconnect vcall
$scope.disconnectVCall = function(){
  tokbox.disconnect();
  $rootScope.callingButton = true;
  $rootScope.callingSpinner = true;
}


$scope.state = false;
$scope.changeVideoState = function() {
        $scope.state = $scope.state === true ? false : true;
        console.log("toggle video is calling");
        console.log($scope.state);

        tokbox.pubVideo($scope.state);

    }  
$scope.audioState = true;
 $scope.changeaudioState = function() {
        $scope.audioState = $scope.audioState === true ? false : true;
        console.log("toggle audio is calling");
        console.log($scope.audiostate);

        tokbox.pubAudio($scope.audioState);

    }    

//update read status
$scope.updateReadStatus = function(sid,rid){
  Msg.updateReadmsg(sid,rid).success(function(data){

    Msg.getUnreadMsg($rootScope.uData.user_id).success(function(data){
      console.log(data);
      $rootScope.mcount = data.count;

    })
  })
}

//Schedule Meeting
$scope.schedule = function() {
  var scheduleData = {
    "meeting_with": $scope.attendee.user_id,
    "meeting_from": $rootScope.uData.user_id,
    "meeting_time": $scope.scheduleTime
  };
  console.log(scheduleData);
  Msg.setSchedule(scheduleData).success(function(response) {
    console.log(response);
    console.log("Meeting Scheduled");
  });
}
});

