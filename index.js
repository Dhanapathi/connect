var https = require("https"); // http server core module
var express = require("express"); // web framework external module
var fs = require('fs');
var bodyParser = require('body-parser');
var path = require('path');
var Sequelize = require('sequelize');
var session = require('express-session');
var multer = require('multer');
var OpenTok = require('opentok');

var auth = require('./routes/auth');
var user = require('./routes/users');
var profile = require('./routes/profiles');
var employment = require('./routes/employments');
var connection = require('./routes/connections');
var skill = require('./routes/skills');
var message = require('./routes/messages');
var qualification = require('./routes/qualifications');
var scheduleMeetings = require('./routes/schedule_meetings');

var ProfileStatus = require('./models/profile.js');


//opentalk api secret
var apiKey ='45696012';
var apiSecret = 'ab6526aad9153f96792de8a2734fa6d5bbc01a74';
var opentok = new OpenTok(apiKey, apiSecret);

var port = 8080;
var httpApp = express();

const options = {
  key: fs.readFileSync('key.key'),
  cert: fs.readFileSync('cert.cert')
};

httpApp.use(session({
    secret: process.env.SESSION_SECRET || '57f3e31d-59e2-4994-b0e9-14d75f4a44f6',
    resave: false,
    saveUninitialized: false,
    cookie: ({
        maxAge: 9000000
    })
}));

var storage = multer.diskStorage({
   destination: function(req, file, cb) {
     console.log("destination");
       cb(null, './uploads/');
   },
   filename: function(req, file, cb) {
       console.log("renamed");
       cb(null, req.session.user.id + '.jpg');
   }
});

var upload = multer({
   storage: storage
});

httpApp.use(bodyParser.json());
httpApp.use(bodyParser.urlencoded({
    extended: true
}));
httpApp.use('/public', express.static(__dirname + '/public'));
httpApp.use('/uploads', express.static(__dirname + '/uploads'));

httpApp.get('/',function (req,res) {
  res.sendFile(path.join(__dirname + '/public/index.html'));
});

httpApp.use('/auth', auth);
httpApp.use('/api/user', user);
httpApp.use('/api/profile', profile);
httpApp.use('/api/employment', employment);
httpApp.use('/api/skill', skill);
httpApp.use('/api/connection', connection);
httpApp.use('/api/message', message.router);
httpApp.use('/api/qualification', qualification);
httpApp.use('/api/schedule', scheduleMeetings);

httpApp.post('/fileupload', upload.any(), function(req, res) {
 console.log("inside fileupload");
   res.send("success");
});

var http = https.createServer(options, httpApp).listen(port, function() {
      console.log('*******************************************\n');
      console.log('Server is listening on port : ' + port + '\n');
      console.log('*******************************************\n');
});
var io = require('socket.io')(http);

//tokbok 

httpApp.get('/api/tokbox/session',function (req,res) {
    console.log("get/session..................");

    opentok.createSession({mediaMode:"routed" , archiveMode:'always'}, function(err, session) {
  if (err) return console.log(err);

var sessionid = session.sessionId;
   // io.emit('newClientConnected', sessionid);
//   sessionList.push(session);
//   console.log(sessionList);
var token = session.generateToken();

var responseObj = {"sessionId": sessionid,"apiKey":apiKey,"token":token};
console.log(responseObj);
res.json(responseObj);
  
});



   
});

httpApp.post('/api/tokbox/session',function (req,res) {
    console.log("post/dash...................................");

    console.log("req.bparams....:  "+req.body.sessionId);

    console.log("req.body..."+req.body.sessionId);

   var sessionI = req.body.sessionId  ;

  var token = opentok.generateToken(sessionI);

   res.json({"apiKey" :apiKey,"token":token,"sessionId":sessionI});
});

//socket IO 

var users = {};
var sockets = {};
var userList = [];

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function isEmpty(objToCheck){
    for(var prop in objToCheck){
        if(objToCheck.hasOwnProperty(prop))
            return false;
    }    
    return true;
}

io.on('connection', function(socket) {

    console.log("socket id connected" +socket.id);

    

    // Register your client with the server, providing your username
    socket.on('init', function(username) {
        users[username] = socket.id;    // Store a reference to your socket ID
        sockets[socket.id] = { username : username, socket : socket };

       ProfileStatus.updateStatus( username, "online", function( updatedProfile ) {
        if( updatedProfile !== null ) {

              if(userList.indexOf(username) === -1){
            userList.push(username);
            io.emit("online_users", userList);
        }
        else{
            console.log("User is already in Online User List");
        }
        
            
        } 
    });



      
        console.log(userList);
        
    });


//connect Page event code here
socket.on('connectpage',function(data){
    console.log(data);
    console.log("socket.connect function is in");
    io.emit("online_users", userList);
})


//

    //disconnect event
    socket.on('disconnect', function() {
      console.log('Got disconnect!' + socket.id + "#####");
     
     if(sockets[socket.id] == null) {
         console.log("user is not available");
     } else {
///////////////////////////////


ProfileStatus.updateStatus( sockets[socket.id].username, "offline", function( updatedProfile ) {
        if( updatedProfile !== null ) {

      console.log("user is available");
        console.log(sockets[socket.id].username + "disconnected");
         userList.remove(sockets[socket.id].username);
         console.log("User is removed from list curretly online users are ..");
         io.emit("online_users", userList);
            
        } 
    });

    
     }

   });


   //logout socket notification
   socket.on('logout',function(data){
        io.emit("online_users", userList);
   })


    // Private message is sent from client with username of person you want to 'private message'
     // Private message is sent from client with username of person you want to 'private message'
    socket.on('private_message', function(data) {
        console.log(data.to+"--------"+data.mesg);
        var messagedata = {
          sender_id : sockets[socket.id].username,
          reciever_id : data.to,
          sent_msg : data.mesg
        }
        console.log(message);
        message.addMessage(messagedata, function(err, response) {
          if(err) {
            io.to(sockets.id).emit(
                'message',
                {
                    message : "Sorry Error Occured...message not send",
                    from : sockets[socket.id].username,
                    user_name: data.user_name
                }
            );
          } else {
            console.log("mesg stored");
            // Lookup the socket of the user you want to private message, and send them your message
            io.to(users[data.to]).emit(
                'message',
                {
                    message : data.mesg,
                    from : sockets[socket.id].username,
                    user_name: data.user_name
                }
            );
          }
        });
    });
    socket.on('video_call', function(data) {
        //console.log(to+"---"+users[to]+"----"+sockets[users[to]]);
        //console.log(users);
        console.log(data);
        console.log(data.to+"--------"+data.sessionId);
        // Lookup the socket of the user you want to private message, and send them your message
        io.to(users[data.to]).emit(
            'video_call_popup',
            {
                sessionId : data.sessionId,
                from : sockets[socket.id].username
            }
        );
    });
    socket.on('reject_video_call', function(data) {
        
        console.log(data);
        console.log(data.to+"--------"+data.reason);
      
        io.to(users[data.to]).emit(
            'reject_video_call_popup',
            {
                reason : data.reason,
                from : sockets[socket.id].username
            }
        );
    });
});

//Execute scheduler file for sending sms reminder
setInterval(function() {
    var exec = require('child_process').exec;
    exec('node scheduler.js', function(error, stdout, stderr) {
        console.log('stdout: ' + stdout);
        console.log('stderr: ' + stderr);
        if (error !== null) {
            console.log('exec error: ' + error);
        }
    });
}, 60000);