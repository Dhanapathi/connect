var express = require('express');
var router = express.Router();
var Qualification = require('../models/qualification.js');

// Get qualification by Id
router.get('/:id', function( req, res ) {
    var id = req.params.id;
    
    Qualification.getQualification( id, function( qualification ) {
        if( qualification !== null ) {
            res.json( qualification );
        } else {
            res.send('false');
        }
    });
});

// Get All Qualifications of a user
router.post('/getQualifications', function( req, res ) {
    var userId = req.body.user_id || req.session.user.id;
    
    Qualification.getAllQualifications( userId, function( qualificationList ) {
        if( qualificationList !== null ) {
            res.json( qualificationList );
        } else {
            res.send('false');
        }
    });
});

// Add new qualification
router.post('/addQualification', function( req, res ) {

    var newQualification = {
        user_id: req.session.user.id,
        university: req.body.university,
        degree: req.body.degree,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        city: req.body.city,
        country: req.body.country,
        description: req.body.description
    }
    
    Qualification.addQualification( newQualification, function( qualification ) {
        if( qualification !== null ) {
            res.json( qualification );
        } else {
            res.send('false');
        }
    });
});

// Update existing qualification
router.post('/updateQualification/:id', function( req, res ) {
    var id = req.params.id;
    var userId = req.session.user.id;
    
    var updateData = {
        university: req.body.university,
        degree: req.body.degree,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        city: req.body.city,
        country: req.body.country,
        description: req.body.description
    }
    
    Qualification.updateQualification( id, userId, updateData, function( updatedQualification ) {
        if( updatedQualification !== null ) {
            res.json( updatedQualification );
        } else {
            res.send('false');
        }
    });
});

// Remove Qualification
router.get('/deleteQualification/:id', function( req, res ) {
    
    var id = req.params.id;
    var userId = req.session.user.id;
    
    Qualification.deleteQualification( id, userId, function( deletedQualification ) {
        if( deletedQualification !== null ) {
            res.json( deletedQualification );
        } else {
            res.send('false');
        }
    });
});

module.exports = router;