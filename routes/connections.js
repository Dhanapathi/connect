var express = require('express');
var router = express.Router();
var async = require('async');

var Connection = require('../models/connection.js');


// Add a new Connection
router.post('/addConnection', function( req, res ) {
     var userId = req.session.user.id;
 
     var connId = req.body.connId;
    
    // INSERTING a entry to db user has a connection to remote user
     var q1 = function (callback) {
          Connection.addConnection( userId, connId, function( connection ) {
          if( connection !== null ) {
              console.log("1st");
              var data = connection;
              } else {
              var data = false;
          }
     });         
     callback();
     }

     // INSERTING a entry to db remote user has a connection to  user
     var q2 = function (callback) {
                Connection.addConnection( connId,userId , function( connection ) {
                if( connection !== null ) {
                    console.log("2nd");
                    var data = connection;
                } else {
                    var data = false;
                }
            });         
                callback();
     }

    async.series([
         function (callback) {
             q1(callback)
         },
          function (callback) {
              q2(callback)
          }
                ],
    // optional callback
        function(err, results){
            if(err){
                console.log("error during sync function");
                console.log(err);
                res.json({"success" : "false"})
            }
             else{
    res.json({"success" : "true"})
                 
             }
                
        // results is now equal to ['one', 'two']
        });
 });


// Get all connection by userId
router.post('/getUserConnections', function( req, res ) {
    var userId = req.body.user_id || req.session.user.id;
    Connection.getAllConnections( userId, function( connectionList ) {
        if( connectionList !== null ) {
            res.json( connectionList );
        } else {
            res.send('false');
        }
    });
});

module.exports = router;