ConnectApp.controller('ProfileController', function($scope, Profile, $location) {
 console.log("ProfileController");

  $scope.getTime = function () {
	  return new Date().getTime();
  }

// getting all profile data skills employments education
 $scope.getProfileData = function () {
	 Profile.getProfile().success(function (data) {
		 $scope.profileData = data;
		 console.log(data);
	 })

	 Profile.getSkills().success(function (data) {
		 $scope.profileSkills = data;
		 console.log(".....skill ");
		 console.log(data);
	 })

	 Profile.getEducations().success(function (data) {
		 $scope.profileEdu = data;
		 console.log(".....Eduction ");
		 console.log(data);
	 })

	 Profile.getEmployments().success(function (data) {
		 $scope.profileEmp = data;
		 console.log(".....emp ");
		 console.log(data);
	 })
 }


  // function for adding new profile 
  $scope.saveProfile = function () {

    Profile.addProfile($scope.profile).success(
				function(data) {
   					console.log("......##....")
					console.log(data);
                    $location.path('/home');

				}).error(function(data){

					console.log("errrr"+data.error);
					$scope.errMessage = data.error;
						
					
				});
  }


	// function for adding education
  $scope.saveEducation = function () {
	  Profile.addEducation($scope.edu).success(function (data) {
		  console.log(data);
		   $location.path('/home/profile');
	  }).error(function (data) {
		  console.log(data);
		  console.log("err in education");
	  });
  }

	// function for adding employment
  $scope.saveEmployment = function () {
	  Profile.addEmployment($scope.emp).success(function (data) {
		  console.log(data);
		   $location.path('/home/profile');
	  }).error(function (data) {
		  console.log("error in emp"+ data);
	  });
	  
  }

	// function for adding skill
  $scope.saveSkill = function () {
	  
	  Profile.addSkill($scope.skill).success(function (data) {
		  console.log(data);
		  $location.path('/home/profile');
	  }).error(function (data) {
		  console.log("error in skill"+params);
	  });
  }

});
