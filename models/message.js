var Sequelize = require('sequelize');
var sequelize = require('../config/dbconfig.js');

/**
* Message Schema
*/
var Message = sequelize.define('message', {
    id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    sender_id: {
      type: Sequelize.STRING,
      allowNull: false
    },
    reciever_id: {
      type: Sequelize.STRING,
      allowNull: false
    },
    sent_msg: {
      type: Sequelize.STRING,
      allowNull: true
    },
    is_read: {
      type: Sequelize.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'messages'
  }
);

sequelize.sync();
module.exports = Message;

// Add new Message
module.exports.addMessage = function( message, callback) {
  console.log("inside dao layer");
    var newMessage = {
        sender_id: message.sender_id,
        reciever_id: message.reciever_id,
        sent_msg: message.sent_msg,
        is_read: 0
    };

    Message.create( newMessage ).then( callback(null, "success")).catch( callback("Error occur", null));
};

// Get messages
module.exports.getMessages = function( message, callback ) {
    var queryMessage = 'SELECT * FROM messages where (sender_id = "' + message.sender_id + '" or sender_id = "' + message.reciever_id + '") and (reciever_id = "' + message.sender_id + '" or reciever_id = "' + message.reciever_id + '")';

    sequelize.query( queryMessage, {
        type: sequelize.QueryTypes.SELECT
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting messages : ' + error );
    });
};

// Get all messages data
module.exports.getAllMessages = function( message, callback ) {
    var queryMessage = 'SELECT distinct reciever_id FROM messages where sender_id = "' + message.sender_id + '" or reciever_id = "' + message.sender_id + '" ORDER BY createdAt DESC';

    sequelize.query( queryMessage, {
        type: sequelize.QueryTypes.SELECT
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting all messages data : ' + error );
    });
};

// Get searched users chat messages data
module.exports.getSearchChat = function( searchCriteria, callback ) {
    var queryMessage = 'SELECT distinct reciever_id FROM messages where sender_id = "' + searchCriteria.sender_id + '" and reciever_id like "%' + searchCriteria.reciever_id + '%"';

    sequelize.query( queryMessage, {
        type: sequelize.QueryTypes.SELECT
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting searched chat data : ' + error );
    });
};

//Update message read status
module.exports.updateReadStatus = function( message, callback ) {

    Message.update({
        is_read: 1
    }, {
        where: {
            sender_id: message.sender_id,
            reciever_id: message.reciever_id,
            is_read: 0
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while updating message read status : ' + error );
    });
};

// Getting unread message count
module.exports.getUnreadCount = function( message, callback ) {

    Message.findAll({
        where: {
            sender_id: message.sender_id,
            reciever_id: message.reciever_id,
            is_read: 0
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting message unread count : ' + error );
    });
};

// Getting unread messages of a user
module.exports.getUnreadMessages = function( message, callback ) {

    Message.findAndCountAll({
        where: {
            reciever_id: message.reciever_id,
            is_read: 0
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting users unread messages : ' + error );
    });
};

// Getting all sender users data
module.exports.getAllSenderUsers = function( message, callback ) {

    Message.findAll({
        where: {
            sender_id: message.email
        }
    }).then( callback ).catch( function( error ) {
        console.log('Database Error while getting all sender users data : ' + error );
    });
};
