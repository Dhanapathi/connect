var express = require('express');
var router = express.Router();
var Profile = require('../models/profile.js');

// Create a new profile
router.post('/newProfile', function( req, res ) {
    
    var userId = req.session.user.id;
    
    var newProfile = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        middle_name: req.body.middle_name,
        gender: req.body.gender,
        date_of_birth: req.body.date_of_birth,
        phone: req.body.phone,
        address: req.body.address,
        pincode: req.body.pincode,
        city: req.body.city,
        country: req.body.country,
        designation: req.body.designation,
        about_me: req.body.about_me,
        industry: req.body.industry
    };
    
    Profile.addProfile( userId, newProfile, function( profile ) {
        if( profile !== null ) {
            res.json( profile );
        } else {
            res.send('false');
        }
    });
});

// Update existing profile
router.post('/updateProfile', function( req, res ) {
    var userId = req.session.user.id;
    
    var updateData = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        middle_name: req.body.middle_name,
        gender: req.body.gender,
        date_of_birth: req.body.date_of_birth,
        phone: req.body.phone,
        address: req.body.address,
        pincode: req.body.pincode,
        city: req.body.city,
        country: req.body.country,
        designation: req.body.country,
        about_me: req.body.about_me,
        industry: req.body.industry
    };
    
    Profile.updateProfile( userId, updateData, function( updatedProfile ) {
        if( updatedProfile !== null ) {
            res.json( updatedProfile );
        } else {
            res.send('false');
        }
    });
});

// Get profile by Id
router.get('/', function( req, res ) {
    var userId = req.session.user.id;
    
    Profile.getProfile( userId, function( profile ) {
        if( profile !== null ){
            res.json( profile );
        } else {
            res.send('false');
        }
    });
});

// Get remote profile
router.post('/remoteProfile', function( req, res ) {
    var remoteUserId = req.body.userId;
    
    Profile.getProfile( remoteUserId, function( remoteProfile ) {
        if( remoteProfile !== null ) {
            res.json( remoteProfile );
        } else {
            res.send('false');
        }
    });
});

// Get all profiles
router.get('/getAllProfiles', function( req, res ) {
    Profile.getAllProfiles( function( profiles ) {
        if( profiles !== null ){
            res.json( profiles );
        } else {
            res.send('false');
        }
    });
});


// get Searched people
router.post('/getSearchPeople', function(req, res) {
    var profile = {
        search_data: req.body.searchText
    }

    Profile.getSearchPeople(profile, function(profile) {
        if (profile != null) {
            res.json(profile);
        }
    });
});

module.exports = router;