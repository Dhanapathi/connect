ConnectApp.controller('RemoteProfileController', function($scope, $routeParams,Profile, $location) {
 console.log("RemoteProfileController");


// getting all profile data skills employments education
 $scope.getRemoteProfileData = function () {
         var id = $routeParams.id;

	 Profile.getRemoteProfile(id).success(function (data) {
		 $scope.profileDataRemote = data;
		 console.log(data);
	 })

 

	 Profile.getRemoteSkills(id).success(function (data) {
		 $scope.profileSkillsRemote = data;
		 console.log(".....skill ");
		 console.log(data);
	 })

	 Profile.getRemoteEducations(id).success(function (data) {
		 $scope.profileEduRemote = data;
		 console.log(".....Eduction ");
		 console.log(data);
	 })

	 Profile.getRemoteEmployments(id).success(function (data) {
		 $scope.profileEmpRemote = data;
		 console.log(".....emp ");
		 console.log(data);
	 })
 }


 $scope.connect = function () {
	 var connId = $scope.profileDataRemote.user_id ;
	 Profile.addConnection(connId).success(function (data) {
		 console.log(data);
		 if(data.success === "true"){
			 $location.path('/home/connect');
		 }
		 else{
			 console.log("some error during connect chk api");
		 }
		 
	 })
 }


 $scope.getTime = function () {
	  return new Date().getTime();
  }
 

});
