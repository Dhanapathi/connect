ConnectApp.factory('ProfilePic', function($rootScope, $http, $q) {
console.log("inside profile pic factory");
	var profilepicFactory = {};

  var hasUserMedia = function() {
    return !!(navigator.getUserMedia || navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia || navigator.msGetUserMedia);
  }

  profilepicFactory.getUserMedia = function() {
    if (hasUserMedia()) {
      navigator.getUserMedia = navigator.getUserMedia ||
          navigator.webkitGetUserMedia || navigator.mozGetUserMedia ||
          navigator.msGetUserMedia;
          $rootScope.video = document.querySelector('video'),
          $rootScope.canvas = document.querySelector('canvas'),
          $rootScope.streaming = false;
      navigator.getUserMedia({
          video: true,
          audio: false
      }, function(stream) {
          $rootScope.video.src = window.URL.createObjectURL(stream);
          $rootScope.streaming = true;
      }, function(error) {
          console.log("Raised an error when capturing:", error);
      });
    };
  }

  profilepicFactory.captureImg = function() {
        if ($rootScope.streaming) {
            $rootScope.canvas.width = $rootScope.video.videoWidth;
            $rootScope.canvas.height = $rootScope.video.videoHeight;
            var context = $rootScope.canvas.getContext('2d');
            context.drawImage($rootScope.video, 0, 0);
            //dataURL = canvas.toDataURL();
        }
      }

  profilepicFactory.getFileData = function() {
    console.log("inside getFileData");
    var Pic= canvas.toDataURL("image/png");
    var blob = dataURItoBlob(Pic);
    var fd = new FormData(document.forms[0]);
    fd.append("canvasImage", blob);
    return fd;
  }

  var dataURItoBlob = function(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i = i +1) {
            ia[i] = byteString.charCodeAt(i);
        }
      return new Blob([ia], {type:mimeString});
    }

    profilepicFactory.upload = function(fd) {
      return $http.post('/fileupload', fd,
			{
				transformRequest: angular.identity,
			  headers: {'Content-Type': undefined}
			})
			.success(function(data,status) {
        console.log(data);
        return data;
      }).error(function(data,err){
        data = "invalid";
        return data;
      })
    }

    return profilepicFactory;
});
