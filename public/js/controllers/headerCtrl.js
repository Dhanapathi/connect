ConnectApp.controller('HeaderController', function($scope,Auth, Profile, Msg,socket, $location, $rootScope) {


  console.log("HeaderController");

//   onhover and hoverout function for logout profile
   $scope.hoverIn = function(){
        this.hoverEdit = true;
    };

    $scope.hoverOut = function(){
        this.hoverEdit = false;
    };

 $scope.getTime = function () {
	  return new Date().getTime();
  }


// function for display user's full name in header popup
   Profile.getProfile().success(function (data) {
     console.log("getting user data");
       $scope.userData = data;
       $rootScope.uData = data;
       // Unread Message counter
        Msg.getUnreadMsg($rootScope.uData.user_id).success(function(data){
             console.log(data);
             $rootScope.mcount = data.count;
           })
   })

    $scope.doLogout = function () {
        console.log("dologot");
        Auth.logout().success(function (data) {
            socket.emit('logout',"x");
            $location.path("/login");
        })
    }


// function for search user from header searchbar
    $scope.searchProfile = function () {

        Profile.searchProfile($scope.searchText).success(function (data) {
        console.log("search data");
        console.log(data);

        $scope.searchProfileData = data;
        $scope.isSearch = true;
        })
        .error(function (data) {
        console.log("No result found");
         });
    }


// function for clear search result from header
    $scope.clearSearch = function () {
        $scope.isSearch = false;
        $scope.searchProfileData = null;
    }
});
